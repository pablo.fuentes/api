module.exports = {
    testMatch: [
        '**/_test_/**/*.spec.ts'
    ],
    testEnvironment: 'node',
    verbose: true,
    "transform": {
        ".(ts|tsx)": "<rootDir>/node_modules/ts-jest/preprocessor.js"
    }
}
