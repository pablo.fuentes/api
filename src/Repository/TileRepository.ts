import {Model} from 'mongoose'
import {ITile, ITileToCreate, ITileToUpdate} from "../Interfaces/Tile"
import {tileModel} from '../Database/models/Tile'

export default class TileRepository {
    private model: Model<ITile> = tileModel

    public get(): ITile[] | {} {
        console.log('Repos board')
        return this.model.find({})
    }

    public getOne(id: string) : ITile | {} {
        console.log(`Retrieving Tile with id  ${id}`)
        return this.model.findById(id)
    }

    public create(tileToCreate: ITileToCreate):  ITile | {}  {
        console.log(`Creting tile`, {tileToCreate})
        return this.model.create(tileToCreate)
    }

    public edit(id: string, updates: ITileToUpdate): ITile | {}  {
        console.log(`Updating tile ${id} with the following data`, {updates})
        return this.model.findOneAndUpdate({_id: id}, {$set: updates})
    }

    public delete(id: string): ITile| {}  {
        console.log(`Removing tile ${id}`)
        return this.model.findOneAndDelete({_id: id},)
    }
}

