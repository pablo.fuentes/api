import {Model} from 'mongoose'
import {IUser} from "../Interfaces/User"
import {userModel} from '../Database/models/User'
import {compare, genSaltSync, hashSync} from 'bcrypt'
import config from 'config'

const salts: number = config.get('user.saltNumber')

export default class UserRepository {
    private model: Model<IUser> = userModel

    public async isValidUser(email: string, pass: string): Promise<IUser | {}> {
        console.log(`Retrieving user with email ${email}`)
        const user = await this.model.findOne({email:email})
        if (!user) return false
        return compare(pass, user.password)
    }

    public getOne(id: string): IUser | {} {
        console.log('Retrieving all boards')
        return this.model.findById(id)
    }

    public async create(email: string, pass: string): Promise<IUser | {}> {
        console.log(`Creating user with email ${email}`)
        const salt = genSaltSync(salts)
        const hashPass = hashSync(pass, salt)
        return this.model.create({email:email, password: hashPass})
    }
}
