import {Model} from 'mongoose'
import {IBoard} from "../Interfaces/Board"
import {boardModel} from '../Database/models/Board'

export default class BoardRepository {
    private model: Model<IBoard> = boardModel

    public get(): IBoard[] | {} {
        console.log('Retrieving all boards')
        return this.model.find({})
    }

    public getOne(id: string): IBoard | object {
        console.log('Retrieving all boards')
        return this.model.findById(id)
    }
}
