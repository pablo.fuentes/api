import {Request, Response} from 'express'
import {UserApplication} from '../Application/UserApplication'

export class UserRouter {
    private userApp: UserApplication

    constructor() {
        this.userApp = new UserApplication()
    }

    public login = async (req: Request, res: Response): Promise<void| Response> => {
        try{
            const email = req.body.email
            const pass = req.body.password
            const token = await this.userApp.login(email, pass)
            if (!token) return res.status(403).send("Not authorized")
            res.status(200).send(token)
        } catch (err) {
            console.log(err)
            res.status(500).send({error: "Unexpected error in login"})
        }
    }
    public signup = async (req: Request, res: Response): Promise<void| Response>  => {
        try{
            const email = req.body.email
            const pass = req.body.password
             await this.userApp.register(email, pass)
            res.status(201).send("User Created")
        } catch (err) {
            console.log(err)
            res.status(500).send({error: "Unexpected error crating user"})
        }
    }
}
