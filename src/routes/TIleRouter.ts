import {Request, Response} from 'express'
import {TileApplication} from "../Application/TileApplication"
import {ITileToCreate, ITileToUpdate} from "../Interfaces/Tile"

export class TileRouter {
    private tile: TileApplication

    constructor() {
        this.tile = new TileApplication()
    }

    public get = async (req: Request, res: Response) => {
        try {
            const tiles = await this.tile.get()

            res.status(200).send(tiles)
        } catch (err) {
            console.log(err)
            res.status(500).send({error: "Unexpected error retrieving tiles"})
        }

    }

    public create = async (req: Request, res: Response): Promise<void| Response>  => {
        try {
            const tileToCreate: ITileToCreate = req.body
            const tiles = await this.tile.create(tileToCreate)
            res.status(200).send(tiles)
        } catch (err) {
            if (err.message === 'Board Not found') return res.status(400).send({error: "Board not foud"})

            console.log(err)
            res.status(500).send({error: "Unexpected error crating tile"})
        }
    }

    public getOne = async (req: Request, res: Response): Promise<void| Response>  => {
        try {
            const tileId = req.params.tileId
            const tile = await this.tile.getOne(tileId)

            if (!tile) return res.status(404).send({message: "Tile not found"})
            res.status(200).send(tile)
        } catch (err) {
            console.log(err)
            res.status(500).send({error: "Unexpected error retrieving tile"})
        }
    }

    public updateOne = async (req: Request, res: Response): Promise<void| Response>  => {
        try {
            const tileId = req.params.tileId
            const dataToUpdate: ITileToUpdate = req.body
            if (!tileId) return res.status(400).send({message: "Missing tile id"}) // todo quitar con validador

            const tile = await this.tile.getOne(tileId)
            if (!tile) return res.status(404).send({message: "Tile not found"})

            const update = await this.tile.updateOne(tileId, dataToUpdate)
            res.status(200).send(update)
        } catch (err) {
            console.log(err)
            res.status(500).send({error: "Unexpected error updating tile"})
        }
    }

    public deleteOne = async (req: Request, res: Response): Promise<void| Response>  => {
        try {
            const tileId = req.params.tileId
            if (!tileId) return res.status(400).send({message: "Missing tile id"}) // todo quitar con el validadot

            const tile = await this.tile.getOne(tileId)

            if (!tile) return res.status(404).send({message: "Tile not found"})
            await this.tile.delete(tileId)
            res.status(201).send()

        } catch (err) {
            console.log(err)
            res.status(500).send({error: "Unexpected error removing tile"})
        }
    }

}

