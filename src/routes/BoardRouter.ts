import {Request, Response} from 'express'
import {BoardApplication} from '../Application/BoardApplication'

export class BoardRouter {
    private board: BoardApplication

    constructor() {
        this.board = new BoardApplication()
    }

    public get = async (req: Request, res: Response): Promise<void> => {
        try {
            const boards = await this.board.get()
            res.status(200).send(boards)
        } catch (err) {
            console.log(err)
            res.status(500).send({error: "Unexpected error retrieving boards"})
        }
    }
}
