import {Router} from 'express'
import {BoardRouter} from "./BoardRouter"
import {TileRouter} from "./TIleRouter"
import {UserRouter} from "./UserRouter"
import {isAuthenticated} from "../middlewares/IsAuthenticated"
import {Validator} from "../middlewares/Validator"

export const router = Router()
const boardRouter = new BoardRouter()
const tileRouter = new TileRouter()
const userRouter = new UserRouter()

const validator = new Validator()
// board
router.get('/', boardRouter.get)



// tiles
router.get('/tile', tileRouter.get)

router.post('/tile/', validator.createTile, isAuthenticated, tileRouter.create)
router.get('/tile/:tileId', tileRouter.getOne)
router.patch('/tile/:tileId', validator.editTile, isAuthenticated, tileRouter.updateOne)
router.delete('/tile/:tileId', isAuthenticated, tileRouter.deleteOne)

// login
router.post('/login', userRouter.login)
router.post('/signup', userRouter.signup)
