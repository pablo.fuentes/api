import {App} from "./App"
import config from 'config'

const port: number = config.get('server.port')
const app = new App()
app.start(port)
    .then(() => {
        console.log("Server running")
    })
    .catch(err => {
        console.error("fatal error", {err})
        process.exit(1)
    })
