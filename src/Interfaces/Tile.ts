import {Document} from "mongoose"

export interface ITile extends Document{
    _id: string,
    name: string,
    description: string,
    status: string,
    board: string,
    created: Date,
    updated: Date
}
export interface ITileToCreate {
    boardId: string,
    status: string,
    title: string,
    description: string
}
export interface ITileToUpdate {
    boardId?: string,
    status?: string,
    title?: string,
    description?: string
}
