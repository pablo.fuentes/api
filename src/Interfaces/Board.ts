import {Document} from "mongoose"

export interface IBoard extends Document{
    _id: string,
    name: string,
    description: string,
    status: string
    created: Date,
    updated: Date
}
