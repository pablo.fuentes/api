import BoardRepository from '../Repository/BoardRepository'
import {IBoard} from "../Interfaces/Board"

export class BoardApplication {
    public repo: BoardRepository

    constructor() {
        this.repo = new BoardRepository()
    }

    public get(): IBoard[] | {} {
        return this.repo.get()
    }
}
