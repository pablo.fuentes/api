import UserRepository from '../Repository/UserRepository'
import config from 'config'
import * as jwt from 'jsonwebtoken'
import {IUser} from "../Interfaces/User"


export class UserApplication {
    public repo: UserRepository
    private secret: string = config.get('token.secret')

    constructor() {
        this.repo = new UserRepository()
    }

    public async login(email: string, pass: string): Promise<{ access_token: string }> { // todo tipar
        const isValid = await this.repo.isValidUser(email, pass)
        if (!isValid) return
        const access_token = jwt.sign({isValid: true}, this.secret, {expiresIn: 60 * 60 * 2})
        return {access_token}
    }

    public async verify(token: string): Promise<boolean> {
        try {
            const decoded = jwt.verify(token, this.secret)
            return decoded !== undefined
        } catch (err) {
            return false
        }
    }

    public register(email: string, password: string): Promise<IUser | {}> {
        return this.repo.create(email, password)
    }
}
