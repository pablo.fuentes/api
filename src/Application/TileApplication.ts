import TileRepository from "../Repository/TileRepository"
import {ITile, ITileToCreate, ITileToUpdate} from "../Interfaces/Tile"
import BoardRepository from "../Repository/BoardRepository"


export class TileApplication {
    public repo: TileRepository
    public boardRepo: BoardRepository

    constructor() {
        this.repo = new TileRepository()
        this.boardRepo = new BoardRepository()

    }

    public get(): ITile[] | {} {
        return this.repo.get()
    }

    public async create(tileToCreate: ITileToCreate): Promise<ITile | {}> {
        const boardId: string = tileToCreate.boardId
        const board = await this.boardRepo.getOne(boardId)
        if (!board){
            throw new Error('Board Not found')
        }
            return this.repo.create(tileToCreate)
    }

    public getOne(id: string): ITile | {} {
        return this.repo.getOne(id)
    }

    public updateOne(id: string, dataToUpdate: ITileToUpdate): ITile | {}  {
        // validar => los campos que le pertenezcan
        return this.repo.edit(id, dataToUpdate)
    }

    public delete(id: string): ITile | {}  {
        return this.repo.delete(id)
    }
}
