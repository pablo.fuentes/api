import {UserApplication} from "../UserApplication"
import config from 'config'
import * as jwt from 'jsonwebtoken'
const fakeUser = {
    "email": "a@b.es",
    "password": "$2b$10$pEeaJS5jxT5GCMQTw4qqsevczUl8xXQbzFXpaKCVzY2iZ9wBAhkOe",
    "created": new Date(),
    "updated": new Date(),
}
jest.mock('../../Repository/UserRepository', () => {
    return jest.fn().mockImplementation(() => {
        return {isValidUser: () => Promise.resolve(true), create:()  =>Promise.resolve(fakeUser)}
    })
})
let app: UserApplication

describe("User application", () => {
    beforeAll(() => {
        app = new UserApplication()
    })
    beforeEach(() => {
        jest.resetAllMocks()
    })

    test("logs in a user", async () => {
        expect(app).toBeInstanceOf(UserApplication)
        const token = await app.login("fake_email", "fake_pass")
        expect(token).toBeDefined()
        expect(token).toHaveProperty('access_token')
        expect(token.access_token).toBeDefined()
    })
    // todo not logs in a user
    test("verifies a token", async () => {
        const secret: string = config.get('token.secret')
        const access_token:string = jwt.sign({isValid: true}, secret, {expiresIn: 60 * 60 * 2})
        const isVerified = await app.verify(access_token)
        expect(isVerified).toBe(true)

    })
    test("not verifies an invalid token", async () => {
        const access_token:string = "fake_token"
        const isVerified = await app.verify(access_token)
        expect(isVerified).toBe(false)

    })
    test("creates a user", async () => {
        expect(app).toBeInstanceOf(UserApplication)
        const create = await app.register("fake_email", "fake_pass")
        expect(create).toBeDefined()
        expect(create).toMatchSnapshot({
            ...fakeUser,
            created: expect.any(Date),
            updated: expect.any(Date)
        })
    })

})
