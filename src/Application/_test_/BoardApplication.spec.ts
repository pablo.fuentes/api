import {BoardApplication} from "../BoardApplication"
import BoardRepository from "../../Repository/BoardRepository"

const fakeBoard = {
    "_id": "board.5de43fbe5b791978f317cc2f",
    "name": "fake_board",
    "status": "fake_status",
    "description": "fake_description",
    "created": new Date(),
    "updated": new Date(),
}
jest.mock('../../Repository/BoardRepository', () => {
    return jest.fn().mockImplementation(() => {
        return {get:() => fakeBoard}
    })
})
let app: BoardApplication

describe("Board application", () => {
    beforeAll(() => {
        app = new BoardApplication()
    })
    beforeEach(() => {
        jest.resetAllMocks()
    })

    test("Returns boards", async () => {
        expect(app).toBeInstanceOf(BoardApplication)
        const boards = await app.get()
        expect(boards).toBeDefined()
        expect(boards).toMatchSnapshot({
            ...fakeBoard,
            created: expect.any(Date),
            updated: expect.any(Date)
        })
    })

})
