import {TileApplication} from '../TileApplication'

const fakeTile = {
    "name": "fake",
    "description": "this is a fake tile",
    "created": new Date(),
    "updated": new Date(),
}
const fakeBoard = {
    "_id": "board.5de43fbe5b791978f317cc2f",
    "name": "fake_board",
    "status": "fake_status",
    "description": "fake_description",
    "created": new Date(),
    "updated": new Date(),
}
jest.mock('../../Repository/TileRepository', () => {
    return jest.fn().mockImplementation(() => {
        return {
            get: () => Promise.resolve([fakeTile]),
            create: () => Promise.resolve(fakeTile),
            getOne: () => Promise.resolve(fakeTile),
            edit: () => Promise.resolve(fakeTile),
            delete: ()=> Promise.resolve(fakeTile)
        }
    })
})
jest.mock('../../Repository/BoardRepository', () => {
    return jest.fn().mockImplementation(() => {
        return {
            getOne: () => Promise.resolve(fakeBoard)
        }
    })
})
let app: TileApplication
describe("Tile app", () => {
    beforeAll(() => {
        app = new TileApplication()
    })
    beforeEach(() => {
        jest.resetAllMocks()
        jest.resetAllMocks()
    })

    test("finds a tile list", async () => {
        expect(app).toBeInstanceOf(TileApplication)
        const tileList = await app.get()
        expect(tileList).toBeDefined()
    })

    test("Get one tile", async () => {
        const tile = await app.getOne("fake_id")
        expect(tile).toBeDefined()
        expect(tile).toMatchSnapshot({
            ...fakeTile,
            created: expect.any(Date),
            updated: expect.any(Date)
        })

    })
    test("creates a tile", async () => {
        const fake_tile = {
            boardId: "fake_board",
            status: "fake",
            title: "fake_title",
            description: "fake_description"
        }
        const created = await app.create(fake_tile)
        expect(created).toBeDefined()
        expect(created).toMatchSnapshot({
            ...fakeTile,
            created: expect.any(Date),
            updated: expect.any(Date)
        })

    })
    test("updates a tile", async () => {
        const fake_tile = {
            boardId: "fake_board",
            status: "fake",
            title: "fake_title",
            description: "fake_description"
        }
        const created = await app.updateOne("fake_tile",fake_tile)
        expect(created).toBeDefined()
        expect(created).toMatchSnapshot({
            ...fakeTile,
            created: expect.any(Date),
            updated: expect.any(Date)
        })
    })
    test("removes a tile", async () => {
        const created = await app.delete("fake_tile")
        expect(created).toBeDefined()
        expect(created).toMatchSnapshot({
            ...fakeTile,
            created: expect.any(Date),
            updated: expect.any(Date)
        })
    })

})
