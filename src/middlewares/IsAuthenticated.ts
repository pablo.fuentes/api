import {NextFunction, Request, Response} from "express"
import {UserApplication} from '../Application/UserApplication'

const userApplication = new UserApplication()

export function isAuthenticated(req: Request, res: Response, next: NextFunction): void {
    // @ts-ignore
    const token: string = req.headers["auth-token"]
    if (!token) res.status(403).send("Not Authenticated")
    const isValid = userApplication.verify(token.replace('Bearer', '').trim()
    )
    if(!isValid) res.status(403).send("Not Authorized")
    next()
}
