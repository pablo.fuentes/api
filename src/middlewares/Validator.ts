import {NextFunction, Request, Response} from "express"
import * as joi from "joi"
export class Validator {

    public createTile(req: Request, res: Response, next: NextFunction) {
        const schema: joi.Schema = joi.object({
            boardId: joi.string().required(),
            name: joi.string().required(),
            status: joi.string().valid(['TODO', "WIP", "RELEASED"]).required(),
            description: joi.string().required()
        })
        return validateObject(req, res, schema, next)
    }

    public editTile(req: Request, res: Response, next: NextFunction) {
        const schema: joi.Schema = joi.object({
            boardId: joi.string(),
            name: joi.string(),
            status: joi.string().valid(['TODO', "WIP", "RELEASED"]),
            description: joi.string()
        })
        return validateObject(req, res, schema, next)
    }
}



function validateObject(req: Request, res: Response, schema: joi.Schema, next: NextFunction): void | Response {
    const isValid = joi.validate(req.body, schema)
    if (isValid.error) {
        return res.status(400).send({error: `Error: ${isValid.error.name}, ${isValid.error.details[0].message}`})
    }
    next()
}
