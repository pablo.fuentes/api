import express from "express"
import * as bodyParser from "body-parser"
import {router} from './routes/index'
import {createTerminus} from '@godaddy/terminus'
import {dbConnection} from './Database/connection'
import * as http from "http"
import config from "config"

const onHealthCheck = () => {
    return Promise.resolve()
}
const onReadinessCheck= () => {
    return new Promise<void>(async (resolve, reject) => {
        dbConnection()
            .then(() => resolve())
            .catch((err) => {
                console.error(err)
                reject()
            })
    })
}

const onSignal = () => () => {
    console.log('server is starting cleanup')
    return Promise.resolve()
}
const onShutdown = () => () => {
    console.log('server is shutting down')
    return Promise.resolve()
}
export class App {

    private readonly app: express.Application
    private serverTimeout: number = config.get('server.timeout')
    private server: any
    constructor() {
        this.app = express()
        this.config()
        this.routes()

    }

    private async config() {
        try{
            this.app.disable('x-powered-by')
            this.app.use(bodyParser.json())
            this.app.use(bodyParser.urlencoded({extended: false}))
            this.server = createTerminus(http.createServer(this.app), {
                timeout: this.serverTimeout,
                signal: 'SIGINT',

                healthChecks: {
                    '/_health/liveness': onHealthCheck,
                    '/_health/readiness': onReadinessCheck
                },
                onSignal: onSignal(),
                onShutdown: onShutdown(),
                logger: console.log
            })
        } catch (e) {
            console.log("error", e)
        }

    }

    private routes(): void {
        console.log("loading routes")
        this.app.use('/api', router)

    }

    public start(port: number): Promise<void> {
        return new Promise<void>(async (resolve) => {
            await dbConnection()
            this.server.listen(port, () => {
                console.log('Server running on port ' + port)
                resolve()
            })
        })
    }

    public stop(): Promise<void> {
        return new Promise<void>((resolve) => {
            this.server.close() // I guess i can make this work, otherwise i wont be able to run acceptance tests
        })
    }


}

