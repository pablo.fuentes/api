import {Model, model, Schema} from 'mongoose'
import {IUser} from "../../Interfaces/User"
const userSchema: Schema = new Schema({
        email: {
            type: String
        },
        password: {
            type: String,
        }
    },
    {
        timestamps: {
            createdAt: 'created',
            updatedAt: 'updated'
        }}
)

export const userModel: Model<IUser> = model('User', userSchema)
