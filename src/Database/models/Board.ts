import {Model, model, Schema} from 'mongoose'
import {IBoard} from "../../Interfaces/Board"
import {v4} from 'uuid'
const boardSchema: Schema = new Schema({
        _id: {
            type: String,
            default: `board.${v4()}`
        },
        name: {
            type: String
        },
        status: {
            type: String,
        },
        description: {
            type: String
        }
    },
    {
        timestamps: {
            createdAt: 'created',
            updatedAt: 'updated'
        }}
)

export const boardModel: Model<IBoard> = model('Board', boardSchema)
