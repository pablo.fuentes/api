import {Model, model, Schema} from 'mongoose'
import {ITile} from "../../Interfaces/Tile"
const tileSchema: Schema = new Schema({
        name: {
            type: String
        },
        status: {
            type: String,
            enum: ['TODO', "WIP", "RELEASED"]
        },
        board: {
            type: String
        },
        description: {
            type: String
        }
    },
    {
        timestamps: {
            createdAt: 'created',
            updatedAt: 'updated'
        }}
)

export const tileModel: Model<ITile> = model('ITile', tileSchema)
