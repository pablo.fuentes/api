import {connect, connection, Connection, disconnect, Mongoose} from 'mongoose'
import config from 'config'

const DATABASE_URL: string = config.get('db.url')

export const dbConnection = (): Promise<Connection | Mongoose | void> => new Promise<Connection | Mongoose | void>((resolve,reject) => {
    if (connection.readyState) {
        return resolve(connection)
    }
    connection.once('connected', (..._) => {
        console.info('mongodb connected' )
        return resolve(connection)
    })

    connection.on('error', error => {
        console.error('mongodb error', { error })
        disconnect()
            .catch(err => {
                console.error('mongodb error disconnecting..', { err })
            })
    })
    return  connect(DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true  })
        .catch((err: any) => {
            console.error('mongodb error connecting..', { err })
            reject(err)
        })
})
