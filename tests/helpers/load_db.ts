import {dbConnection} from '../../src/Database/connection'
import {boardModel} from '../../src/Database/models/Board'
import {tileModel} from '../../src/Database/models/Tile'
import {v4} from 'uuid'

export async function loadDb() {
    await boardModel.deleteMany({})
    await tileModel.deleteMany({})
    await dbConnection()
    await boardModel.create({
        _id: `board.${v4()}`,
        name: 'fake_board',
        status: "fake_status",
        description: "fake_description"
    })
}
