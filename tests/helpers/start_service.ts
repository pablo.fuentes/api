import {App} from '../../src/App'

const app = new App()

export async function startHttp() {
    const port = 4000
    await app.start(port)
        .catch(error => {
            console.error("fatal error", {error})
            process.exit(1)
        })
}

export async function stopHttp() {
    await app.stop()
}
