import request from 'request-promise'
import config from 'config'

export function requestBoards() {
    const port: number = config.get('server.port')
    const endpoint = `http://localhost:${port}/api/`
    return request(endpoint)
}
