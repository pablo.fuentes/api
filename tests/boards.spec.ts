import {loadDb} from "./helpers/load_db"
import {startHttp, stopHttp} from "./helpers/start_service"
import {requestBoards} from "./helpers/request_boards"
describe("Retrieve boards", () => {
    beforeAll(async () => {
        await loadDb()
        await startHttp()
    })
    afterAll(async () => {
        await stopHttp()
    })
    test("Call to get boards retrieves boards in db", async () => {
        const boards = await requestBoards()
        expect(boards).not.toBeUndefined()
        expect(boards.length).not.toBeUndefined()
        expect(boards[0]).toMatchSnapshot()
    })

})
