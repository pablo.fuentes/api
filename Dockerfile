FROM node:10

COPY package.json .
RUN npm install --production

COPY dist dist

COPY config config

USER node

CMD ["node", "dist/index"]

